import React, { useState } from "react";
import { Link, useNavigate } from "react-router-dom";

const FormLogin = ({ changeur = true }) => {
  const [loginForm, setLoginForm] = useState({
    email: "",
    password: "",
  });

  const handleChange = (e) => {
    setLoginForm({
      ...loginForm,
      [e.target.name]: e.target.value,
      id: Date.now(),
    });
  };
  const navigate = useNavigate();

  const handleSubmit = (e) => {
    e.preventDefault();

    if (
      loginForm.email == "test@example.com" &&
      loginForm.password == "password"
    ) {
      // setIsLoggedIn(true);
      changeur = false;
      navigate("/dashboard");
      // changer = true;
    } else {
      alert("try agin");
      e.stopPropagation();
    }
  };

  return (
    <>
      <h1>Sign In</h1>
      <p>Sign in by entering information below</p>
      <form className="row g-3" onSubmit={handleSubmit}>
        <div className="col-12 form-floating">
          <input
            type="email"
            className="form-control"
            id="email"
            value={loginForm.email}
            name="email"
            onChange={handleChange}
            required
          />
          <label htmlFor="email" className="form-label">
            Email
          </label>
        </div>
        <div className="col-12 form-floating">
          <input
            type="password"
            className="form-control"
            id="password"
            value={loginForm.password}
            name="password"
            onChange={handleChange}
            required
          />
          <label htmlFor="password" className="form-label">
            Password
          </label>
        </div>
        <div className="col-12 d-flex justify-content-around">
          <button className="btn btn-primary shadow" type="submit">
            Sign In
          </button>
        </div>
        <div>
          <p>
            Don't have an account ?<Link to={"/signup"}>Sign up</Link>
          </p>
        </div>
      </form>
    </>
  );
};

export default FormLogin;
