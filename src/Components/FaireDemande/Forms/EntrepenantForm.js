import React, { useState, useEffect } from "react";

const EntrepenantForm = ({ entrepenantForms, entrepenantFormsData }) => {
  const [entrepenantForm, setEntrepenantForm] = useState({
    address: "",
    sector: "",
    ...entrepenantForms,
  });

  const handleChange = (e) => {

    console.log(e.target.name, e.target.value)
    setEntrepenantForm({
      [e.target.name]: e.target.value,
      ...entrepenantForm,
    });
  };
  

  useEffect(() => {
    entrepenantFormsData(entrepenantForm);
  }, [entrepenantForm]);

  return (
    <div>
      <form class="mt-3 row g-3">
        <div class="col-6">
          <label for="adresse" class="form-label">
            Adresse de l'activité
          </label>
          <input
            type="text"
            class="form-control"
            id="adresse"
            name="address"
            value={entrepenantForm.address}
            onChange={handleChange}
            required
          />
        </div>
        <div class="col-6">
          <label for="secteurActivite" class="form-label">
            Secteur de l'activité
          </label>
          <input
            type="text"
            class="form-control"
            id="secteurActivite"
            name="sector"
            onChange={handleChange}
            value={entrepenantForm.sector}
            required
          />
        </div>
      </form>
    </div>
  );
};

export default EntrepenantForm;
