import React, { useState, useEffect } from "react";
import { FaPhoneFlip, FaWhatsapp } from "react-icons/fa6";
import { GrMail } from "react-icons/gr";

const ContactForm = ({contactForm, contactFormData}) => {
  const [contact, setContact] = useState({
    phone: "",
    whatsapp: "",
    email: "",
    city: "",
    ...contactForm
  });

  const handleChange = (e) => {
    console.log(e.target.name, e.target.value)
    setContact({
      ...contact,
      [e.target.name]: e.target.value,
      id: Date.now(),
    });
  };

  useEffect(()=>{
    contactFormData(contact)
  },[contact])

  return (
    <div>
      <form class="row g-3">
        <div class="col-6">
          <label for="mobile" class="form-label">
            Telephone mobile
          </label>
          <div className="d-flex py-1 px-2  justify-content-center bg-white align-items-center border rounded">
            <FaPhoneFlip className="" />
            <input
              type="number"
              class="form-control border-0"
              id="mobile"
              name="phone"
              value={contact.phone}
              onChange={handleChange}
              required
            />
          </div>
        </div>
        <div class="col-6">
          <label for="whatsapp" class="form-label">
            Whatsapp
          </label>
          <div className="d-flex py-1 px-2  justify-content-center bg-white align-items-center border rounded">
            <FaWhatsapp />
            <input
              type="number"
              class=" border-0 form-control"
              id="whatsapp"
              name="whatsapp"
              onChange={handleChange}
              value={contact.whatsapp}
              required
            />
          </div>
        </div>
        <div class="col-6">
          <label for="email" class="form-label">
            Adresse email
          </label>
          <div className="d-flex py-1 px-2 justify-content-center bg-white align-items-center border rounded">
            <GrMail />
            <input
              type="email"
              class="border-0 form-control"
              id="email"
              name="email"
              onChange={handleChange}
              value={contact.email}
              required
            />
          </div>
        </div>
        <div class="col-6">
          <label for="commune" class="form-label">
            Commune
          </label>
          <div className="p-1 bg-white border rounded">
            <input
              type="text"
              class=" border-0 form-control"
              id="commune"
              name="city"
              onChange={handleChange}
              value={contact.city}
              required
            />
          </div>
        </div>
      </form>
    </div>
  );
};

export default ContactForm;
