import React, { useState, useEffect } from "react";
import { FaLink } from "react-icons/fa6";
import { FaPhoneFlip } from "react-icons/fa6";
import { GrMail } from "react-icons/gr";

const StartupForm = ({ startUpForms, startUpFormsData }) => {
  const [startUpForm, setStartUpForm] = useState({
    denomination: "",
    sector: "",
    entreprise_email: "",
    ncc: "",
    nrc: "",
    entreprise_phone: "",
    address: "",
    website: "",
    ...startUpForms,
  });

  const handleChange = (e) => {
    setStartUpForm({
      ...startUpForm,
      [e.target.name]: e.target.value,
    });
  };

  useEffect(() => {
    startUpFormsData(startUpForm);
  }, [startUpForm]);

  return (
    <div>
      <form class="row mt-3 g-3">
        <div class="col-6">
          <label for="denomination" class="form-label">
            *Dénomination (StartUp)
          </label>
          <div className="d-flex py-1 px-2  justify-content-center bg-white align-items-center border rounded">
            <input
              type="text"
              class="form-control border-0"
              id="denomination"
              name="denomination"
              value={startUpForm.denomination}
              onChange={handleChange}
              required
            />
          </div>
        </div>
        <div class="col-6">
          <label for="secteurActivite" class="form-label">
            *Secteur d'activité (StartUp)
          </label>
          <div className="d-flex py-1 px-2  justify-content-center bg-white align-items-center border rounded">
            <input
              type="text"
              class="form-control border-0"
              id="secteurActivite"
              name="sector"
              value={startUpForm.sector}
              onChange={handleChange}
              required
            />
          </div>
        </div>
        <div class="col-6">
          <label for="numeroRC" class="form-label">
            Numero de registre de commerce (StartUp)
          </label>
          <div className="d-flex py-1 px-2  justify-content-center bg-white align-items-center border rounded">
            <input
              type="text"
              class="form-control border-0"
              id="numeroRC"
              name="nrc"
              value={startUpForm.nrc}
              onChange={handleChange}
            />
          </div>
        </div>
        <div class="col-6">
          <label for="numeroCC" class="form-label">
            Numero de compte contribuable (StartUp)
          </label>
          <div className="d-flex py-1 px-2  justify-content-center bg-white align-items-center border rounded">
            <input
              type="number"
              class="form-control border-0"
              id="numeroCC"
              name="ncc"
              value={startUpForm.ncc}
              onChange={handleChange}
            />
          </div>
        </div>

        <div class="col-6">
          <label for="telephone" class="form-label">
            Numéro de télephone (StartUp)
          </label>
          <div className="d-flex py-1 px-2  justify-content-center bg-white align-items-center border rounded">
            <FaPhoneFlip />
            <input
              type="number"
              class="form-control border-0 "
              id="telephone"
              name="entreprise_phone"
              value={startUpForm.entreprise_phone}
              onChange={handleChange}
              required
            />
          </div>
        </div>

        <div class="col-6">
          <label for="email" class="form-label">
            Adresse email (StartUp)
          </label>
          <div className="d-flex py-1 px-2  justify-content-center bg-white align-items-center border rounded">
            <GrMail />
            <input
              type="email"
              class="form-control border-0"
              id="email"
              name="entreprise_email"
              onChange={handleChange}
              value={startUpForm.entreprise_email}
              required
            />
          </div>
        </div>

        <div class="col-6">
          <label for="adresse" class="form-label">
            Aresse du siège (StartUp)
          </label>
          <div>
            <div className="d-flex py-1 px-2  justify-content-center bg-white align-items-center border rounded">
              <input
                type="text"
                class="form-control border-0"
                id="adresse"
                name="address"
                value={startUpForm.address}
                onChange={handleChange}
              />
            </div>
          </div>
        </div>

        <div class="col-6">
          <label for="siteWeb" class="form-label">
            *Site web (StartUp)
          </label>
          <div className="d-flex py-1 px-2  justify-content-center bg-white align-items-center border rounded">
            <FaLink />
            <input
              type="text"
              class="form-control border-0"
              id="siteWeb"
              name="website"
              onChange={handleChange}
              value={startUpForm.website}
            />
          </div>
        </div>
      </form>
    </div>
  );
};

export default StartupForm;
