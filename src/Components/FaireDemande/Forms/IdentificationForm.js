import React, { useState, useEffect } from "react";

const IdentificationForm = ({ identificationForm, identificationFormData }) => {
  const [identification, setIdentification] = useState({
    id: "",
    firstname: "",
    lastname: "",
    gender: "",
    agerange: "",
    handicap: "",
    diaspora: "",
    ...identificationForm,
  });

  const handleChange = (e) => {
    console.log(e.target.name, e.target.value);
    setIdentification({
      ...identification,
      [e.target.name]: e.target.value,
      id: Date.now(),
    });
  };

  useEffect(() => {
    identificationFormData(identification);
  }, [identification]);

  return (
    <div>
      <form class="row g-3 p-3">
        <div class="col-6">
          <label for="nom" class="form-label">
            Mon nom
          </label>
          <input
            type="text"
            class="form-control"
            id="name"
            name="firstname"
            value={identification.firstname}
            onChange={handleChange}
            required
          />
        </div>
        <div class="col-6">
          <label for="prenom" class="form-label">
            Mon prénom
          </label>
          <input
            type="text"
            class="form-control"
            id="prenom"
            name="lastname"
            onChange={handleChange}
            value={identification.lastname}
            required
          />
        </div>
        <div class="col-6">
          <label for="sexe" class="form-label">
            Mon sexe
          </label>
          <select
            class="form-select"
            id="sexe"
            name="gender"
            onChange={handleChange}
            required
          >
            <option selected disabled value="">
              Choisissez...
            </option>
            <option
              value="Femme"
              selected={identification.gender == "Femme" ? true : false}
            >
              Femme
            </option>
            <option
              value="Homme"
              selected={identification.gender === "Homme" ? true : false}
            >
              Homme
            </option>
          </select>
        </div>

        <div class="col-6">
          <label for="age" class="form-label">
            Ma tranche d'age
          </label>
          <select
            class="form-select"
            id="age"
            name="agerange"
            onChange={handleChange}
            required
          >
            <option selected disabled value="">
              Choisissez...
            </option>
            <option
              value="16 - 35 ans"
              selected={identification.agerange == "16 - 35 ans" ? true : false}
            >
              16 - 35 ans
            </option>
            <option
              value="36 - 40 ans"
              selected={identification.agerange == "36 - 40 ans" ? true : false}
            >
              36 - 40 ans
            </option>
            <option
              value="41 ans et plus"
              selected={
                identification.agerange == "41 ans et plus" ? true : false
              }
            >
              41 ans et plus
            </option>
          </select>
        </div>

        <div className="col-6">
          <p>Êtes-vous en situation d'handicap ?</p>
          <div className="d-flex gap-5">
            <div class="form-check">
              <input
                class="form-check-input"
                type="radio"
                name="handicap"
                value="true"
                onChange={handleChange}
                id="ouiHandicap"
                checked={identification.handicap == "true" ? true : false}
              />
              <label class="form-check-label" for="ouiHandicap">
                Oui
              </label>
            </div>
            <div class="form-check">
              <input
                class="form-check-input"
                type="radio"
                name="handicap"
                value="false"
                onChange={handleChange}
                id="nonHandicap"
                checked={identification.handicap == "false" ? true : false}
              />
              <label class="form-check-label" for="nonHandicap">
                Non
              </label>
            </div>
          </div>
        </div>

        <div className="col-6">
          <p>Êtes-vous de la diaspora ?</p>
          <div className="d-flex gap-5">
            <div class="form-check">
              <input
                class="form-check-input"
                type="radio"
                name="diaspora"
                value="true"
                onChange={handleChange}
                id="ouiDiaspora"
                checked={identification.diaspora == "true" ? true : false}
              />
              <label class="form-check-label" for="ouiDiaspora">
                Oui
              </label>
            </div>
            <div class="form-check">
              <input
                class="form-check-input"
                type="radio"
                name="diaspora"
                value="false"
                onChange={handleChange}
                id="nonDiaspora"
                checked={identification.diaspora == "false" ? true : false}
              />
              <label class="form-check-label" for="nonDiaspora">
                Non
              </label>
            </div>
          </div>
        </div>
      </form>
    </div>
  );
};

export default IdentificationForm;
