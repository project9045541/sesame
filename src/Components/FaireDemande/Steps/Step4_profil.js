import React, { useState, useEffect } from "react";
import EntrepenantForm from "../Forms/EntrepenantForm";
import PmeForm from "../Forms/PmeForm";
import StartupForm from "../Forms/StartupForm";

const Step4Profil = ({step4Profil, step4ProfilData, forms, formsData}) => {
  const [choice, setChoice] = useState(1);
  const [profilForm, setProfilForm] = useState({
    category: "",
    ...step4Profil
  });

  const [entrepenantForms, setEntrepenantForms] = useState({});

  const [pmeForms, setPmeForms] = useState({});

  const [startUpForms, setStartUpForms] = useState({});

  function entrepenantFormsData(data){
    formsData({...forms, ...data})
    setEntrepenantForms(data)
  }

  function pmeFormsData(data){
    formsData({...forms, ...data})
    setPmeForms(data)
  }

  function startUpFormsData(data){
    formsData({...forms, ...data})
    setStartUpForms(data)
  }

  const handleChange = (e) => {
    setProfilForm({
      ...profilForm,
      [e.target.name]: e.target.value
    });
    if ([e.target.value] == "Porteur de projet") {
      setChoice(1);
    } else if ([e.target.value] == "Entreprenant") {
      setChoice(2);
    } else if ([e.target.value] == "PME") {
      setChoice(3);
    } else if ([e.target.value] == "Startup") {
      setChoice(4);
    }
  };

  useEffect(()=>{
    step4ProfilData(profilForm)
  },[profilForm])

  return (
    <>
      <form>
        <div className="col">
          <p className="text-start">Êtes vous ?</p>
          <div className="d-flex gap-3">
            <div class="form-check">
              <input
                class="form-check-input"
                type="radio"
                name="category"
                value="Porteur de projet"
                onChange={handleChange}
                checked={profilForm.category == "Porteur de projet" ? true: false}
                id="porteur_de_projet"
                required
              />
              <label class="form-check-label" for="porteur_de_projet">
                Porteur de projet
              </label>
            </div>
            <div class="form-check">
              <input
                class="form-check-input"
                type="radio"
                name="category"
                value="Entreprenant"
                onChange={handleChange}
                checked={profilForm.category == "Entreprenant" ? true: false}
                id="entreprenant"
                required
              />
              <label class="form-check-label" for="entreprenant">
                Entreprenant
              </label>
            </div>
            <div class="form-check">
              <input
                class="form-check-input"
                type="radio"
                name="category"
                value="PME"
                checked={profilForm.category == "PME" ? true: false}
                onChange={handleChange}
                id="pme"
                required
              />
              <label class="form-check-label" for="pme">
                PME
              </label>
            </div>
            <div class="form-check">
              <input
                class="form-check-input"
                type="radio"
                name="category"
                value="Startup"
                checked={profilForm.category == "Startup" ? true: false}
                onChange={handleChange}
                id="startup"
                required
              />
              <label class="form-check-label" for="startup">
                Startup
              </label>
            </div>
          </div>
          {choice == 2 && (
            <>
              <EntrepenantForm entrepenantForms={entrepenantForms} entrepenantFormsData={entrepenantFormsData} />
            </>
          )}
          {choice == 3 && (
            <>
              <PmeForm pmeForms={pmeForms} pmeFormsData={pmeFormsData}/>
            </>
          )}
          {choice == 4 && (
            <>
              <StartupForm startUpForms={startUpForms} startUpFormsData={startUpFormsData}/>
            </>
          )}
        </div>
      </form>
    </> 
  );
};

export default Step4Profil;
