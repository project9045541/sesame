import React, { useState, useEffect } from "react";

const Step1Identification = ({step1IdentificationData, step1Identification}) => {
  const [choice, setChoice] = useState(false);
  const [besoinForm, setBesoinForm] = useState({
    type: "",
    information: "",
    ...step1Identification
  });

  const handleChange = (e) => {
    setBesoinForm({
      ...besoinForm,
      [e.target.name]: e.target.value,
      id: Date.now(),
    });
    
    if ([e.target.value] == "Information" || besoinForm.type == "Information") {
      setChoice(true);
    } else {
      setChoice(false);
    }
  };

  useEffect(()=>{
    if (besoinForm.type == "Information") {
      setChoice(true);
    } else {
      setChoice(false);
    }
  },[besoinForm.type])

  useEffect(()=>{
    step1IdentificationData(besoinForm)
  },[besoinForm])

  return (
    <>
      <form>
        <div className="col-md-3">
          <div className="d-flex gap-3">
            <div class="form-check">
              <input
                class="form-check-input"
                type="radio"
                name="type"
                value="Information"
                onChange={handleChange}
                checked={besoinForm.type == "Information" ? true: false}
                id="information"
                required
              />
              <label class="form-check-label" for="information">
                Information
              </label>
            </div>
            <div class="form-check">
              <input
                class="form-check-input"
                type="radio"
                name="type"
                value="Formalisation"
                onChange={handleChange}
                id="formalisation"
                checked={besoinForm.type == "Formalisation" ? true: false}
                required
              />
              <label class="form-check-label" for="formalisation">
                Formalisation
              </label>
            </div>
            <div class="form-check">
              <input
                class="form-check-input"
                type="radio"
                name="type"
                value="Formation"
                onChange={handleChange}
                checked={besoinForm.type == "Formation" ? true: false}
                id="formation"
                required
              />
              <label class="form-check-label" for="formation">
                Formation
              </label>
            </div>
            <div class="form-check">
              <input
                class="form-check-input"
                type="radio"
                name="type"
                value="Financement"
                checked={besoinForm.type == "Financement" ? true: false}
                onChange={handleChange}
                id="financement"
                required
              />
              <label class="form-check-label" for="financement">
                Financement
              </label>
            </div>
          </div>
          {choice && (
            <>
              <p>*Que Voulez vous savoir ?</p>
              <textarea
                name="information"
                id="message"
                onChange={handleChange}
                value={besoinForm.information}
                cols="100"
                rows="4"
              ></textarea>
            </>
          )}
        </div>
      </form>
    </>
  );
};

export default Step1Identification;
