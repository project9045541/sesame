import React, { useState, useEffect } from "react";
import { BsDashLg } from "react-icons/bs";
import Step1Identification from "./Step1_Identification";
import IdentificationForm from "../Forms/IdentificationForm";
import ContactForm from "../Forms/ContactForm";
import Step4Profil from "./Step4_profil";
import axios from "axios";
import { registerUrl } from "../../../Url";
import { useNavigate } from "react-router-dom";

const Stepper = () => {

  const [stepper, setStepper] = useState(1);
  const [step1Identification, setStep1Identification] = useState({});
  const [identificationForm, setIdentificationForm] = useState({});
  const [contactForm, setContactForm] = useState({});
  const [step4Profil, setStep4Profil] = useState({});
  const [forms, setForms] = useState({});
  //const registerurl ="http://localhost:8000/api/register"
  const [loading, setLoading] = useState(false);
  const user = JSON.parse(window?.localStorage.getItem("userData")?? "{}");
  const navigate = useNavigate();

  useEffect(()=>{

    if(user && user?.id){
      navigate('/')
    }
  }, [])

  const handleNext = () => {
    if (stepper >= 1 && stepper < 4) {
      setStepper(stepper + 1);
    }
  };
  const handlePrevious = () => {
    if (stepper >= 2 && stepper <= 4) {
      setStepper(stepper - 1);
    }
  };

  function formsData(data){
    console.log(data, "all data")
    setForms(data);
  }

  function step1IdentificationData(data) {
    setStep1Identification(data);
  }

  function identificationFormData(data) {
    setIdentificationForm(data);
  }
  function contactFormData(data) {
    setContactForm(data);
  }
  function step4ProfilData(data) {
    setStep4Profil(data);
  }

  function handleSubmit() {
    const data = {
      ...step1Identification,
      ...identificationForm,
      ...contactForm,
      ...step4Profil,
      ...forms

    }

    axios
        .post(registerUrl, data)
        .then((response) => {
          console.log(response);
          setLoading(true)
        })
        .catch((err) => {
          console.log(err);
          setLoading(false)
        });

    console.log(data, "all")
  }

  return (
    <div className="mb-5">
      <div className="steps">
        <span
          className={`${
            stepper == 1
              ? "activeStep"
              : stepper > 1
              ? "doneStep"
              : "unDoneStep"
          } px-4 py-2 `}
        >
          1- J'identifie mon besoin
        </span>
        <BsDashLg className="text-white" />
        <span
          className={`${
            stepper == 2
              ? "activeStep"
              : stepper > 2
              ? "doneStep"
              : "unDoneStep"
          } px-4 py-2 `}
        >
          2- Je m'identifie
        </span>
        <BsDashLg className="text-white" />
        <span
          className={`${
            stepper == 3
              ? "activeStep"
              : stepper > 3
              ? "doneStep"
              : "unDoneStep"
          } px-4 py-2 `}
        >
          3- Mes contacts personnels
        </span>
        <BsDashLg className="text-white" />
        <span
          className={`${
            stepper == 4
              ? "activeStep"
              : stepper > 4
              ? "doneStep"
              : "unDoneStep"
          } px-4 py-2 `}
        >
          4- Mon profil
        </span>
      </div>
      <div className="bg-white text-start rounded p-4 mt-3">
        {loading && (<div className="rendu mt-3">Votre demande a été envoyé avec succès. Veuillez vérifier votre boite mail pour la validation de votre compte!</div>)}
        {!loading && (
          <>
            <div className="rendu mt-3">
              {stepper == 1 && (
                <Step1Identification
                  step1Identification={step1Identification}
                  step1IdentificationData={step1IdentificationData}
                />
              )}
              {stepper == 2 && (
                <IdentificationForm
                  identificationForm={identificationForm}
                  identificationFormData={identificationFormData}
                />
              )}
              {stepper == 3 && (
                <ContactForm
                  contactForm={contactForm}
                  contactFormData={contactFormData}
                />
              )}
              {stepper == 4 && (
                <Step4Profil
                  step4Profil={step4Profil}
                  step4ProfilData={step4ProfilData}
                  forms={forms}
                  formsData={formsData}
                />
              )}
            </div>

            <div className="my-3">
              {stepper == 1 ? (
                <div className="text-end">
                  <button className="btn-step" onClick={handleNext}>
                    Suivant
                  </button>
                </div>
              ) : stepper > 1 && stepper < 4 ? (
                <div className="d-flex justify-content-between">
                  <button className="btn-step" onClick={handlePrevious}>
                    Retour
                  </button>
                  <button className="btn-step" onClick={handleNext}>
                    Suivant
                  </button>
                </div>
              ) : (
                <div className="d-flex justify-content-between">
                  <button className="btn-step" onClick={handlePrevious}>
                    Retour
                  </button>
                  <button className="btn-soumission" onClick={handleSubmit}>
                    Soumettre
                  </button>
                </div>
              )}
            </div>
          </>
        ) }
      </div>
    </div>
  );
};

export default Stepper;
