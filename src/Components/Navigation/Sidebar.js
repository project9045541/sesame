import React from 'react';
import Navigation from './Navigation';

const Sidebar = () => {
    return (
        <>
            <div className='enteteNav bg-primaryss d-flex justify-content-center align-items-center'>
                <p style={{color: "#034359 !important"}} className='logo fw-bold display-4 text-whites'><span className=''>Se</span>same</p> 
            </div>
            <div className='d-flex justify-content-center '>
                <div className='px-3 py-1 navigation '>
                    <p className='px-3 mb-0 mt-3'>Accueil</p>
                    <Navigation/>
                </div>
            </div>
        </>
    );
}

export default Sidebar;
