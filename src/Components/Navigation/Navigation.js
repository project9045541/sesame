import React from "react";
import BtnNav from "./BtnNav";
import { FcDocument } from "react-icons/fc";
import { useNavigate } from "react-router-dom";

const Navigation = () => {

  const user = JSON.parse(window?.localStorage.getItem("userData")?? "{}");
  //const user = window.localStorage.getItem("userData");
  console.log(user?.roles, "roles")
  const navigate = useNavigate()

  return (
    <>
      {/* <BtnNav
        titre="Elèves"
        icone={<i class="fa-solid fa-graduation-cap"></i>}
        lien="student"
      />
      <BtnNav
        titre="Professeurs"
        icone={<i class="fa-solid fa-person-chalkboard"></i>}
        lien="teacher"
      />
      <BtnNav
        titre="Notes"
        icone={<i class="fa-solid fa-pen-to-square"></i>}
        lien="note"
      />
      <BtnNav
        titre="Filières"
        icone={<i class="fa-solid fa-building-columns"></i>}
        lien="filiere"
      />
      <BtnNav
        titre="Matières"
        icone={<i class="fa-solid fa-book-bookmark"></i>}
        lien="matiere"
      />
      <BtnNav
        titre="Coefficients"
        icone={<i class="fa-solid fa-calculator"></i>}
        lien="coefficient"
      /> */}

      {user && user.roles === "CLIENT" && (
        <BtnNav
        titre="Completer demande"
        icone={<FcDocument />}
        lien="completer-une-demande"
      />
      )}

      {user && user.roles != "CLIENT" && (
        <BtnNav
        titre="Liste des demandes"
        icone={<FcDocument />}
        lien="list-des-demandes"
      />
      )}

      {user && (
        <button class="btn text-start btn-link w-100 pb-0 mt-2" type="button" onClick={()=>{
          window.localStorage.clear()
          navigate('/login')
        }}>
        <FcDocument /> {"Se déconnecter"}
        </button>
      )}

    </>
  );
};

export default Navigation;
