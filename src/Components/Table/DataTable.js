import React,{useEffect, useState} from "react";
import { useNavigate } from "react-router-dom";
import axios from "axios";
import { MDBDataTable } from "mdbreact";
import { usersUrl } from "../../Url";
const Datatable = () => {
  const navigate = useNavigate();

  const handleDetailClick = (name) => {
    //navigate(`/details/${name}`);
  };

  const [users, setUsers] = useState([]);
  const storedToken = window.localStorage.getItem("token");
  console.log(storedToken, "storedToken")

  const user = JSON.parse(window?.localStorage.getItem("userData")?? "{}");
  useEffect(()=>{

    if(!user){
      navigate('/login')
    }
    if(user && user.roles == "CLIENT"){
      navigate('/')
    }
  }, [])

  useEffect(()=>{
    axios
        .get(usersUrl, {
          headers: {
            Authorization: `Bearer ${storedToken}`
          }
        })
        .then((response) => {
          console.log(response);
          setUsers(response.data.data)
        })
        .catch((err) => {
          console.log(err);
        });
  },[])

  

  const datas = {
    columns: [
      {
        label: "Nom",
        field: "firstname",
        sort: "asc",
        width: 150,
      },
      {
        label: "Prenom",
        field: "lastname",
        sort: "asc",
        width: 270,
      },
      {
        label: "Email",
        field: "email",
        sort: "asc",
        width: 200,
      },
      {
        label: "Numero",
        field: "id",
        sort: "asc",
        width: 100,
      },
      {
        label: "Type",
        field: "type",
        sort: "asc",
        width: 150,
      },
      {
        label: "Category",
        field: "category",
        sort: "asc",
        width: 100,
      },
      {
        label: "Actions",
        field: "actions",
        width: 100,
      },
    ],
    rows: [
      {
        firstname: "Sesame",
        lastname: "Admin",
        email: "admin@gmail.com",
        id: "01",
        type: "Information",
        category: "PME",
        // actions: (
        //   // <button onClick={() => handleDetailClick("Tiger Nixon")}>
        //   //   Détail
        //   // </button>
        // ),
      },
      {
        firstname: "Sesame",
        lastname: "Root",
        email: "root@gmail.com",
        id: "02",
        type: "Formation",
        category: "Startup",
      }
    ],
  };

  const [data, setData] = useState(datas);

  useEffect(()=>{
    if(users?.length > 0){
      
      const newData = [];
      for (const user of users) {
        newData.push({
          firstname: user.firstname,
          lastname: user.lastname,
          email: user.email,
          id: user.id,
          type: user.type,
          category: user.category
        })
      }

      setData({columns: data.columns, rows: newData})
    }
  },[users])

  const rows = data.rows.map((rowData, index) => {
    return {
      ...rowData,
      actions: (
        <button
          className="btn-details px-3 py-1 shadow d-flex justify-content-center"
          onClick={() => handleDetailClick(rowData.name)}
        >
          Détail
        </button>
      ),
      id: index,
    };
  });

  return <MDBDataTable striped bordered small data={{ ...data, rows }} />;
};

export default Datatable;
