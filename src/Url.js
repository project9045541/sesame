const loginUrl = "http://localhost:8000/api/login";
const registerUrl = "http://localhost:8000/api/register"
const logoutUrl = "http://localhost:8000/api/logout"
const usersUrl = "http://localhost:8000/api/user-by-state/all/5000"

export {loginUrl, registerUrl, logoutUrl, usersUrl}