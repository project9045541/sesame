import React, {useEffect} from "react";
import { Route, Routes } from "react-router-dom";
import Home from "./Components/Home";
import Sidebar from "./Components/Navigation/Sidebar";
import StudentsPage from "./Pages/StudentsPage";
import CoefPage from "./Pages/CoefPage";
import FilieresPage from "./Pages/FilieresPage";
import MatieresPage from "./Pages/MatieresPage";
import NotesPage from "./Pages/NotesPage";
import TeachersPage from "./Pages/TeachersPage";
import Header from "./Components/Navigation/Header";
import EleveUpdate from "./Components/Eleves/EleveUpdate";
import CoefUpdate from "./Components/Coefficients/CoefUpdate";
import FiliereUpdate from "./Components/Filieres/FiliereUpdate";
import MatiereUpdate from "./Components/Matieres/MatiereUpdate";
import NoteUpdate from "./Components/Notes/NoteUpdate";
import ProfUpdate from "./Components/Profs/ProfUpdate";
import Step1Identification from "./Components/FaireDemande/Steps/Step1_Identification";
import ContactForm from "./Components/FaireDemande/Forms/ContactForm";
import Stepper from "./Components/FaireDemande/Steps/Stepper";
import Datatable from "./Components/Table/DataTable";
import { useLocation, useNavigate } from "react-router-dom";
import DetailPage from "./Pages/DetailPage";
import EnvoiDocumentPage from "./Pages/EnvoiDocumentPage";
import LoginSesamePage from "./Pages/LoginSesamePage";

const App = () => {
  const params = useLocation();
  const user = window.localStorage.getItem("userData");
  console.log(user)
  const navigate = useNavigate();
  
  useEffect(()=>{
    console.log("user", user)
    if(!user && params.pathname != "/login" && params.pathname != "/faire-une-demande"){
      navigate('/login')
    }
    if(user && user?.id){
      navigate('/')
    }

  }, [])

  if (params.pathname == "/login") {
    return (
      <div className="d-flex demandePage justify-content-center">
        <div className="col-6 text-center  ">
          <div className="px-5 mt-1">
            <div className="text-center">
              <img src="/image/LOGO_SESAME2.png" className="w-50" alt="" />
            </div>
            <div></div>
            <Routes>
              <Route path="login" element={<LoginSesamePage />}></Route>
            </Routes>
          </div>
        </div>
      </div>
    );
  }
  if (params.pathname == "/faire-une-demande") {
    return (
      <div className="d-flex demandePage justify-content-center">
        <div className="col-8 text-center  ">
          <div className="px-5 mt-1">
            <div className="text-center">
              <img src="/image/LOGO_SESAME2.png" className="w-25" alt="" />
            </div>
            <div>
              <h1 className="text-white fs-3 fw-bold fst-italic my-5 ">
                Formulaire d'inscription et de demande
              </h1>
            </div>
            <Routes>
              <Route path="faire-une-demande" element={<Stepper />}></Route>
            </Routes>
          </div>
        </div>
      </div>
    );
  }
  return (
    <div className="d-flex">
      <div className="col-2">
        <Sidebar />
      </div>

      <div className="col app">
        <div className="enteteApp mb-4 bg-white">
          <Header />
        </div>
        <div className="px-5 mt-5">
          <Routes>
            <Route index element={<Home />} />
            <Route path="/" element={<Home />} />
            {/* <Route path="student" element={<StudentsPage />}>
              <Route path=":id" element={<EleveUpdate />} />
            </Route>
            <Route path="coefficient" element={<CoefPage />}>
              <Route path=":id" element={<CoefUpdate />} />
            </Route>
            <Route path="filiere" element={<FilieresPage />}>
              <Route path=":id" element={<FiliereUpdate />} />
            </Route>
            <Route path="matiere" element={<MatieresPage />}>
              <Route path=":id" element={<MatiereUpdate />} />
            </Route> */}
            {/* <Route path="note" element={<NotesPage />}>
              <Route path=":id" element={<NoteUpdate />} />
            </Route>
            <Route path="teacher" element={<TeachersPage />}>
              <Route path=":id" element={<ProfUpdate />} />
            </Route> */}
            <Route path="faire-une-demande" element={<Stepper />}></Route>
            <Route path="list-des-demandes" element={<Datatable />}></Route>
            <Route path="connexion" element={<LoginSesamePage />}></Route>
            {user && user.roles === "CLIENT" && (
              <Route
              path="completer-une-demande"
              element={<EnvoiDocumentPage />}
              ></Route>
            )}

            {user && user.roles != "CLIENT" && (
              <Route
              path="completer-une-demande"
              element={<EnvoiDocumentPage />}
              ></Route>
            )}
            
            <Route path="/details-demande/:name" component={<DetailPage />} />
          </Routes>
        </div>
      </div>
    </div>
  );
};

export default App;
