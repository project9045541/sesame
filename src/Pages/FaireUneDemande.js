import React from "react";
import Stepper from "../Components/FaireDemande/Steps/Stepper";

const FaireUneDemande = () => {
  return (
    <div className="">
      <div className="col-8 d-flex mt-3 justify-content-center ">
        <div>
          <Stepper />
        </div>
      </div>
    </div>
  );
};

export default FaireUneDemande;
