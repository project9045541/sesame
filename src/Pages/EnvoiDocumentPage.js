import React, { useState } from "react";

const EnvoiDocumentPage = () => {
  const [form, setForm] = useState({
    id: "",
    financement: "",
    pdfFile: "",
    imageFile: "",
  });

  const handleChange = (e) => {
    const { name, value, type, files } = e.target;

    if (type === "file") {
      setForm({
        ...form,
        [name]: files[0],
        id: Date.now(),
      });
    } else {
      setForm({
        ...form,
        [name]: value,
        id: Date.now(),
      });
    }
  };

  // const handleChange = (e) => {
  //   setForm({ ...form, [e.target.name]: e.target.value, id: Date.now() });
  // };

  const handleImageChange = (e) => {
    const file = e.target.files[0];
    setForm({ ...form, imageFile: URL.createObjectURL(file) });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    console.log(form.imageFile);
  };
  return (
    <div>
      <form onSubmit={handleSubmit} class="row g-3">
        <div class="col-6">
          <label for="financement" class="form-label">
            Financement
          </label>
          <input
            type="text"
            class="form-control border-0"
            id="financement"
            name="financement"
            value={form.financement}
            onChange={handleChange}
            required
          />
        </div>
        <div class="col-6">
          <label for="pdfFile" class="form-label">
            Doc pdf
          </label>

          <input
            type="file"
            class=" border-0 form-control"
            id="pdfFile"
            name="pdfFile"
            accept=".pdf"
            onClick={handleChange}
            value={form.pdfFile}
            required
          />
        </div>
        <div class="col-6">
          <label for="imageFile" class="form-label">
            Image
          </label>

          <input
            type="file"
            class="border-0 form-control"
            id="imageFile"
            name="imageFile"
            // accept="image/*"
            onClick={handleImageChange}
            value={form.imageFile}
            required
          />
        </div>
        <button type="submit" className="btn-submit">
          Soumettre
        </button>
      </form>
    </div>
  );
};

export default EnvoiDocumentPage;
