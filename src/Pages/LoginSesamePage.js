import React, { useState, useEffect } from "react";
import axios from "axios";
import { loginUrl } from "../Url";
import {
  MDBBtn,
  MDBContainer,
  MDBCard,
  MDBCardBody,
  MDBCol,
  MDBRow,
  MDBInput,
  MDBCheckbox,
  MDBIcon,
} from "mdb-react-ui-kit";
import { useNavigate } from "react-router-dom";

const LoginSesamePage = () => {
  const [login, setLogin] = useState({
    email: "",
    password: "",
  });
  const navigate = useNavigate();
  const user = JSON.parse(window?.localStorage.getItem("userData")?? "{}");

  useEffect(()=>{

    if(user && user?.id){
      navigate('/')
    }
  }, [])

  const handleChange = (e) => {
  
    setLogin({
      ...login,
      [e.target.name]: e.target.value,
    });
  };

  const handleSubmit = async(e) => {
    e.preventDefault();
    axios
      .post(loginUrl, login)
      .then(async response => {
        window.localStorage.setItem("token", response.data.accessToken)
        window.localStorage.setItem('userData', JSON.stringify(response.data.userData))
        navigate("/")
      })
      .catch(err => {
        alert("Une erreur s'est produite")
        console.log(err)
      })
  };

  return (
    <div>
      <MDBContainer fluid>
        <MDBCard
          className="mx-5 mb-5 p-5 shadow-5"
          style={{
            background: "hsla(0, 0%, 100%, 0.8)",
            backdropFilter: "blur(30px)",
          }}
        >
          <MDBCardBody className="p-3 text-center">
            <h2 className="fw-bold mb-5">Se connecter</h2>

            <form onSubmit={handleSubmit}>
              <div className="mb-3">
                <input
                  type="email"
                  class=" border-0 form-control"
                  id="email"
                  name="email"
                  onChange={handleChange}
                  value={login.email}
                  required
                />
                <label for="email" class="form-label">
                  Email
                </label>
              </div>
              <div className="mb-3">
                <input
                  type="password"
                  class=" border-0 form-control"
                  id="password"
                  name="password"
                  onChange={handleChange}
                  value={login.password}
                  required
                />
                <label for="password" class="form-label">
                  Mot de passe
                </label>
              </div>
              <button className="w-100 connexion rounded">Connexion</button>
            </form>

            {/* <MDBBtn onClick={handleSubmit} className="w-100 mb-4" size="md">
              Connexion
            </MDBBtn> */}
          </MDBCardBody>
        </MDBCard>
      </MDBContainer>
    </div>
  );
};

export default LoginSesamePage;
