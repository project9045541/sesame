import React from "react";
import "../Styles/login.css";
import FormLogin from "../Components/FormLogin";
import { useNavigate } from "react-router-dom";

const LoginPage = ({ changeur }) => {
  const navigate = useNavigate();

  return (
    <div className="login-back d-flex justify-content-center align-items-center">
      <div className="col-5 text-center bg-white rounded-4 p-5 shadow">
        <FormLogin changeur={changeur} />
      </div>
    </div>
  );
};

export default LoginPage;
